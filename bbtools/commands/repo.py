import os
from bbtools.deserializers import Repository, User
from bbtools.utils import to_repo

from . import Command

class Commands(Command):
	def list(self, args):
		pl = self.request('/user/')

		user = User(pl.get('user'))
		repos = map(Repository, pl.get('repositories'))

		self.render('repos/list.tmpl', {
			'user': user, 'repos': repos
		})

	def search(self, args):
		pl = self.request('/repositories/?name=%s' % (args.query,))

		repos = map(Repository, pl.get('repositories'))

		self.render('repos/search.tmpl', {
			'query': args.query, 'repos': repos
		})

	def show(self, args):
		repo = self.request('/repositories/'+to_repo(args.repo))

		self.render('repos/show.tmpl', {'repo': repo})

	def clone(self, args):
		repo_name = to_repo(args.repo)
		repo = Repository(self.request('/repositories/'+repo_name))
		
		if repo.scm == 'git':
			cmd = 'git clone https://%s@bitbucket.org/%s.git'
		else:
			cmd = 'hg clone https://%s@bitbucket.org/%s'

		os.system(cmd % (self.cfg.get('user').get('username'), repo_name))

def hookargs(command_sub_parsers):
	list_ = command_sub_parsers.add_parser('list', help='list repos you have access to')
	list_.set_defaults(subcmd='list')

	search = command_sub_parsers.add_parser('search', help='search for a repo by name')
	search.set_defaults(subcmd='search')
	search.add_argument('query')

	show = command_sub_parsers.add_parser('show', help='show repo details')
	show.set_defaults(subcmd='show')
	show.add_argument('repo')

	clone = command_sub_parsers.add_parser('clone', help='clone a repo, dvcs-independent')
	clone.set_defaults(subcmd='clone')
	clone.add_argument('repo')

import os
import sys
from configobj import ConfigObj

def get_config():
	fn = os.path.expanduser('~/.bbtools')
	if not os.path.isfile(fn):
		sys.exit('You need a ~/.bbtools config file of the form:\n'
			 '[user]\n'
			 'username = <your_bb_username>\n'
			 'password = <your_bb_password>\n')

	return ConfigObj(fn)

def to_repo(s):
	if '/' in s:
		return s
	
	cfg = get_config()

	return cfg.get('user').get('username')+'/'+s

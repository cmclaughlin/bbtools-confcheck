class JSONObject(dict):
	def __init__(self, obj):
		super(JSONObject, self).__init__(obj)
	
	def __getattr__(self, key):
		return self.get(key)

	def __repr__(self):
		return '<%s>' % self.__class__.__name__

class Repository(JSONObject):
	pass

class Issue(JSONObject):
	pass

class User(JSONObject):
	pass
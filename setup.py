import sys

from setuptools import setup

install_requires = ['requests', 'configobj>4.7.1', 'fabulous', 'Jinja2']

if sys.version_info[:2] < (2, 7):
    install_requires.append('importlib')

setup(
    name='bbtools',
    version='0.1',
    description="Bitbucket command line tools",
    long_description=open("README.rst").read(),
    author="Sam Tardif & Jesper Noehr",
    author_email="jesper@noehr.org",
    url="https://bitbucket.org/samtardif/bbtools",
    license="BSD",
    keywords="cli bitbucket git mercurial",
    packages=['bbtools', ],
    include_package_data=True,
		package_data={'': ['commands/*.py', 'commands/templates/*/*.tmpl'], },
    entry_points={'console_scripts': ['bb = bbtools:main', ]},
    install_requires=install_requires,
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: End Users/Desktop",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Topic :: Communications",
        "Topic :: Internet",
        "Topic :: Software Development",
        "Topic :: Terminals",
        "Topic :: Utilities",
    ],
)
